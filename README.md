# Initiation Data

> Découvrir le processus des traitements de données numériques,
> en réalisant une cartographie interactive des points d'apport
> volontaires de la métropole de Grenoble

Le notebook Jupyter [cartographie_pav_session.ipynb](cartographie_pav_session.ipynb)
contient l'énoncé et l'activité.

Le notebook Jupyter [cartographie_pav.ipynb](cartographie_pav.ipynb)
contient l'énoncé, l'activité et la solution.

## Utilisation

1. `pipenv install -d`
2. `pipenv shell`
3. `jupyter notebook`
